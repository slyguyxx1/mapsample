﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;

namespace MapSampleCS481
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class MapSamplePage : ContentPage
    {

        public MapSamplePage()
        {
            InitializeComponent();

            PopulatePicker();


        }


        ObservableCollection<Pin> collection = new ObservableCollection<Pin>();



        public void PlaceAMarker(Pin selectedPin)
        {
            var position = selectedPin.Position; // Latitude, Longitude

            string pinLabel = selectedPin.Label;
            string pinAddress = selectedPin.Address;
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = pinLabel,
                Address = pinAddress
            };


            MySampleMap.Pins.Add(pin);
        }

        private void PopulatePicker()
        {
            
            var staplescenterPin = new Pin()
            {
                Type = PinType.Place,
                Position = new Position(34.0430175, -118.2694428),
                Label = "Staples Center",
                Address = "1111 S Figueroa, St, Los Angeles, CA 90015",

            };

            var SixflagsPin = new Pin()
            {
                Type = PinType.Place,
                Position = new Position(34.4251361, -118.5980093),
                Label = "Six Flags Magic Mountain",
                Address = "26101 Magic Mountain Pkwy, Valencia, CA 91355",
            };

            var disneylandPin = new Pin()
            {
                Type = PinType.Place,
                Position = new Position(33.8120918, -117.9211629),
                Label = "DisneyLand",
                Address = "1313 Disneyland Dr, Anaheim, CA 92802",
            };

            var knottsPin = new Pin()
            {
                Type = PinType.Place,
                Position = new Position(33.8443038, -118.0024152),
                Label = "Knotts Berry Farm",
                Address = "8030 Beach Blvd, Buena Park, CA 90620",
            };

            var inAndOutTemecula = new Pin()
            {
                Type = PinType.Place,
                Position = new Position(33.4954381, -117.1587975),
                Label = "In and Out Temecula pkway",
                Address = "27700 Jefferson Ave, Temecula, CA 92590",
            };

            collection.Add(staplescenterPin);
            collection.Add(SixflagsPin);
            collection.Add(disneylandPin);
            collection.Add(knottsPin);
            collection.Add(inAndOutTemecula);







            Dictionary<string, Pin> pinToAdd = new Dictionary<string, Pin>()
            {
                {"StaplesCenter", staplescenterPin },
                {"Six Flags", SixflagsPin},
                {"DisneyLand", disneylandPin},
                {"Knotts Berry Farm", knottsPin},
                {"In and Out", inAndOutTemecula},

            };

            foreach (var item in pinToAdd)
            {
                SamplePicker.Items.Add(item.Key);
            }
            SamplePicker.SelectedIndex = 0;
        }

        public void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {


            if (SamplePicker.SelectedIndex == -1)
            {
                //error message
            }
            else
            {

                int indexOfPicker = SamplePicker.SelectedIndex;

                if(indexOfPicker == 0)
                {
                    var staplescenterPin = new Pin()
                    {
                        Type = PinType.Place,
                        Position = new Position(34.0430175, -118.2694428),
                        Label = "Staples Center",
                        Address = "1111 S Figueroa, St, Los Angeles, CA 90015",

                    };

                    var mapLocation = MapSpan.FromCenterAndRadius(staplescenterPin.Position,Distance.FromMiles(.5));
                    MySampleMap.MoveToRegion(mapLocation);
                    PlaceAMarker(staplescenterPin);

                }

                if(indexOfPicker == 1)
                {
                    var SixflagsPin = new Pin()
                    {
                        Type = PinType.Place,
                        Position = new Position(34.4251361, -118.5980093),
                        Label = "Six Flags Magic Mountain",
                        Address = "26101 Magic Mountain Pkwy, Valencia, CA 91355",
                    };

                    var mapLocation = MapSpan.FromCenterAndRadius(SixflagsPin.Position, Distance.FromMiles(.3));
                    MySampleMap.MoveToRegion(mapLocation);
                    PlaceAMarker(SixflagsPin);


                }
                if (indexOfPicker == 2)
                {
                    var disneylandPin = new Pin()
                    {
                        Type = PinType.Place,
                        Position = new Position(33.8120918, -117.9211629),
                        Label = "DisneyLand",
                        Address = "1313 Disneyland Dr, Anaheim, CA 92802",
                    };

                    var mapLocation = MapSpan.FromCenterAndRadius(disneylandPin.Position, Distance.FromMiles(.3));
                    MySampleMap.MoveToRegion(mapLocation);
                    PlaceAMarker(disneylandPin);


                }
                if (indexOfPicker == 3)
                {
                    var knottsPin = new Pin()
                    {
                        Type = PinType.Place,
                        Position = new Position(33.8443038, -118.0024152),
                        Label = "Knotts Berry Farm",
                        Address = "8030 Beach Blvd, Buena Park, CA 90620",
                    };

                    var mapLocation = MapSpan.FromCenterAndRadius(knottsPin.Position, Distance.FromMiles(.3));
                    MySampleMap.MoveToRegion(mapLocation);
                    PlaceAMarker(knottsPin);


                }
                if (indexOfPicker == 4)
                {
                    var inAndOutTemecula = new Pin()
                    {
                        Type = PinType.Place,
                        Position = new Position(33.5150016, -117.1611338),
                        Label = "In and Out Temecula pkway",
                        Address = "27700 Jefferson Ave, Temecula, CA 92590",
                    };

                    var mapLocation = MapSpan.FromCenterAndRadius(inAndOutTemecula.Position, Distance.FromMiles(.3));
                    MySampleMap.MoveToRegion(mapLocation);
                    PlaceAMarker(inAndOutTemecula);


                }



            }
        }



        void Handle_satelite(object sender, System.EventArgs e)
        {
            MySampleMap.MapType = MapType.Satellite;

        }



        void Handle_hybrid(object sender, System.EventArgs e)
        {
            MySampleMap.MapType = MapType.Hybrid;

        }


        void Handle_street(object sender, System.EventArgs e)
        {
            MySampleMap.MapType = MapType.Street;

        }
    }
}